const platforms = {
  0: 'AWS',
  1: 'Azure',
  2: 'GCP',
  3: 'Alibaba',
  4: 'Other Platform'
};

export default platforms;