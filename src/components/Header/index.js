import React from 'react';
import HeaderSection from './HeaderSection';
import HeaderNav from './HeaderNav';

const Header = (props) => {

  return (
    <header
      className="App-header"
      data-parallax="scroll"
      data-speed="0.5"
    >
      <HeaderNav></HeaderNav>
      {!props.disableHeaderSection && <HeaderSection 
        sectionHeading={props.sectionHeading}
      />}
    </header>
  );
}

export default Header;