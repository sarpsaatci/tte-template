import React from 'react';

const HeaderSection = (props) => {
  if(props.sectionHeading)
  return (
    <section style={{marginTop: '14vh', paddingTop: '2rem', minHeight: '13vh'  }} className=" text-center d-flex align-items-center">
        <div className="container">
        {props.sectionHeading &&
          <h1 className="display-5">{props.sectionHeading}</h1>
        }          
        </div>
        </section>
  );
  return(<div></div>);
}

export default HeaderSection;