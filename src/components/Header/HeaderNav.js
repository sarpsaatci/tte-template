/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../images/logo.png';
import { navbarFixed } from './HeaderNav.module.scss';

const HeaderNav = (props) => {
  return (
    <div className={`row ${navbarFixed} m-0 p-0`}>
      <div className="col-12 p-0">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container" >
        <div className="col-6 col-lg-1">
        <Link to='/#home'>
          <a href=""
              ><img
                width="100%"
                alt="logo"
                src={logo}
              />
            </a>
          </Link>
          </div>
          <div className="col col-md-1">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#site-nav"
            aria-controls="site-nav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          </div>

          <div style={{background: 'white'}} className="col col-lg-9 col-md-12 collapse navbar-collapse" id="site-nav">
            <ul className="navbar-nav text-sm-left ml-auto">
              <li className="nav-item App-link">
                <Link to='/about'><a className="nav-link" href="">About</a></Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      </div>
    </div>
  );
}

export default HeaderNav;