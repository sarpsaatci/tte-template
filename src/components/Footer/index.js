/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Link } from 'react-router-dom';

const Footer = (props) => {
  return (
    <div className="section bg-light" id="footer">
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <h4>Company</h4>
            <ul className="list-unstyled footer-links ml-1">
              <Link to='/about'>
                <li><a href="">About</a></li>
              </Link>
            </ul>
          </div>
          <div className="col-sm-2">
            <a href="#app-top" className="btn btn-sm btn-outline-primary-maxspotter ml-1">Go to Top</a>
          </div>
        </div>
        <div className=" text-center mt-4">
          <small className="text-muted"
            >{`Copyright © ${new Date().getFullYear()} All rights reserved.`}
          </small>
        </div>
      </div>
    </div>
  );
}

export default Footer;