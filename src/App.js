import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.scss';

import About from './pages/About';
import withScrollToTop from './components/withScrollToTop';
import Home from './pages/Home';

const RouteWithScrollToTop = withScrollToTop(Route);

function App() {
  return (
    <div id='app-top' className="App">
      <Router>
        <Switch>
          <RouteWithScrollToTop path="/about">
            <About />
          </RouteWithScrollToTop>
          <RouteWithScrollToTop path="/">
            <Home />
          </RouteWithScrollToTop>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
