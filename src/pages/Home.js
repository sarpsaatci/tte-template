/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { getIp } from '../service/service';

function Home(props) {
  const [ipAddress, setIpAddress] = useState('');

  // works like componentDidMount
  useEffect(() => {
    return getIp()
    .then((res) => {
      if (res && res.data && res.data.ip) {
        setIpAddress(res.data.ip);
      } else {
        setIpAddress('Could not get ip');
      }
    })
    .catch((err) => setIpAddress('Could not get ip'));
  }, []);

  /* works like getDerivedStateFromProps
  
    useEffect(() => {
    return getIp()
    .then((res) => {
      if (res && res.data && res.data.ip) {
        setIpAddress(res.data.ip);
      } else {
        setIpAddress('Could not get ip');
      }
    })
    .catch((err) => setIpAddress('Could not get ip'));
  }, [props]);

  */
  
  return (
    <div>
    <Header sectionHeading='Home' />
    <div className="align-items-center my-lg text-center">
      <div className="container">
        <div className="container">
          <h4>{ipAddress}</h4>
          <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          </p>
          <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          </p>
          <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          </p>
          <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book. It has survived not only five centuries, but also the leap 
          into electronic typesetting, remaining essentially unchanged. It was
          </p>
        </div>
      </div>
    </div>
    <Footer></Footer>
    </div>
  )
}

export default Home;
