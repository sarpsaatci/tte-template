import axios from 'axios';

const backendUrl = process.env.REACT_APP_BACKEND_URL;

export const getIp = () => {
  return new Promise((resolve, reject) => {
    try {
     const res = axios.get(`${backendUrl}/?format=json`);
     return resolve(res);
    } catch(err) {
      return reject(err);
    }
  })
}

export default getIp;
